﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace cuesearcher_winforms
{
    public partial class cueSarcherMainForm : Form
    {
        private const string TITLE_INDICATION = "TITLE";

        public cueSarcherMainForm()
        {
            InitializeComponent();
        }

        private void findSong(string filepath)
        {
            char[] delimiters = {' ','\t'}; // определяем разделители

            string artist = new String(' ',1);
            string album = new String(' ', 1);

            int titlefirst = 1; 
            int flag = 0;
            StreamReader cuefile = new StreamReader(filepath, Encoding.Default);

            while (!cuefile.EndOfStream && flag == 0)
            {
                string substr = cuefile.ReadLine(); // читаем очередную строку из файла

                substr=substr.Trim(' ','\t'); // режем лишнее из строки. не совсем раскурил как работает, потому делал методом тыка
                string[] words = substr.Split(delimiters); // пилим строку на слова

                if (words[0] == TITLE_INDICATION) //нашли заголовок 
                {
                    if (titlefirst == 1) // из-за особенностей строение cue, чтобы не переписывать переменную много раз за 1 файл
                    {
                        titlefirst = 0; 
                        album = getStringFromSplit(words); // запоминаем название альбома
                    }
                    else if (string.Compare(getStringFromSplit(words), this.songNameTxtBox.Text, true) == 0)
                    {
                        flag = 1; // бинго!!

                        substr = cuefile.ReadLine(); // читаем файл дальше
                        substr = substr.Trim(' ', '\t');
                        words = substr.Split(delimiters); // вычленяем имя испольнителя(группы)
                        artist = words[1].Trim('"');
                    }
                }
            }
            cuefile.Close();

            if (flag==1) // если нашли совпадение
            searchResGridView.Rows.Add(this.songNameTxtBox.Text, album, artist, filepath); // то пишем его в датагрид
        }

        private string getStringFromSplit(string[] words) // разбить-то строку разбили, а вот обратно уже костыль
        {
            StringBuilder strout=new StringBuilder("");

            for (int i = 1; i < words.Length; i++)
            {             
                strout.Append(words[i].Trim('"'));
                strout.Append(" ");
            }

            return strout.ToString().TrimEnd();
        }

        private void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Process.Start(this.searchResGridView.CurrentCell.Value.ToString());//пытаемся открыть файл программой по умолчанию
            }
            catch
            {

            }

        }

        private void folderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int i = this.searchResGridView.CurrentRow.Cells[3].Value.ToString().LastIndexOf(@"\");
            System.Diagnostics.Process.Start(this.searchResGridView.CurrentRow.Cells[3].Value.ToString().Substring(0, i));
        }

        private void searchBtn_Click(object sender, EventArgs e)
        {
            List<string> dirs = FileSearcher.GetFilesRecursive(pathTxtBox.Text); // ищем пути всех cue файлов  
            foreach (string paths in dirs)
                findSong(paths); // передаем пути на обработку
        }

        private void getPathBtn_Click(object sender, EventArgs e)
        {
            if (this.selectFolderDialog.ShowDialog() == DialogResult.OK)
                this.pathTxtBox.Text = this.selectFolderDialog.SelectedPath; 
        }
    }

    
}
