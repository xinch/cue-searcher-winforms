﻿namespace cuesearcher_winforms
{
    partial class cueSarcherMainForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.songNameTxtBox = new System.Windows.Forms.TextBox();
            this.selectFolderDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.pathTxtBox = new System.Windows.Forms.TextBox();
            this.searchResGridView = new System.Windows.Forms.DataGridView();
            this.Songcol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.albumcol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.artistcol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pathcol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.searchResMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.folderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchBtn = new System.Windows.Forms.Button();
            this.getPathBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.searchResGridView)).BeginInit();
            this.searchResMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // songNameTxtBox
            // 
            this.songNameTxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.songNameTxtBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.songNameTxtBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.FileSystem;
            this.songNameTxtBox.Location = new System.Drawing.Point(20, 23);
            this.songNameTxtBox.Name = "songNameTxtBox";
            this.songNameTxtBox.Size = new System.Drawing.Size(336, 20);
            this.songNameTxtBox.TabIndex = 0;
            this.songNameTxtBox.Text = "name of song";
            // 
            // pathTxtBox
            // 
            this.pathTxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pathTxtBox.Location = new System.Drawing.Point(20, 57);
            this.pathTxtBox.Name = "pathTxtBox";
            this.pathTxtBox.Size = new System.Drawing.Size(336, 20);
            this.pathTxtBox.TabIndex = 1;
            // 
            // searchResGridView
            // 
            this.searchResGridView.AllowUserToAddRows = false;
            this.searchResGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.searchResGridView.BackgroundColor = System.Drawing.SystemColors.Window;
            this.searchResGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.searchResGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.searchResGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.searchResGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Songcol,
            this.albumcol,
            this.artistcol,
            this.pathcol});
            this.searchResGridView.Location = new System.Drawing.Point(1, 93);
            this.searchResGridView.Name = "searchResGridView";
            this.searchResGridView.ReadOnly = true;
            this.searchResGridView.RowHeadersVisible = false;
            this.searchResGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.searchResGridView.Size = new System.Drawing.Size(463, 115);
            this.searchResGridView.TabIndex = 4;
            this.searchResGridView.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentDoubleClick);
            // 
            // Songcol
            // 
            this.Songcol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Songcol.HeaderText = "Song";
            this.Songcol.Name = "Songcol";
            this.Songcol.ReadOnly = true;
            // 
            // albumcol
            // 
            this.albumcol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.albumcol.HeaderText = "Album";
            this.albumcol.Name = "albumcol";
            this.albumcol.ReadOnly = true;
            // 
            // artistcol
            // 
            this.artistcol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.artistcol.HeaderText = "Artist";
            this.artistcol.Name = "artistcol";
            this.artistcol.ReadOnly = true;
            // 
            // pathcol
            // 
            this.pathcol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.pathcol.ContextMenuStrip = this.searchResMenuStrip;
            this.pathcol.HeaderText = "Path";
            this.pathcol.Name = "pathcol";
            this.pathcol.ReadOnly = true;
            // 
            // searchResMenuStrip
            // 
            this.searchResMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.folderToolStripMenuItem});
            this.searchResMenuStrip.Name = "contextMenuStrip1";
            this.searchResMenuStrip.Size = new System.Drawing.Size(108, 26);
            this.searchResMenuStrip.Text = "Folder";
            // 
            // folderToolStripMenuItem
            // 
            this.folderToolStripMenuItem.Name = "folderToolStripMenuItem";
            this.folderToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.folderToolStripMenuItem.Text = "Folder";
            this.folderToolStripMenuItem.Click += new System.EventHandler(this.folderToolStripMenuItem_Click);
            // 
            // searchBtn
            // 
            this.searchBtn.Location = new System.Drawing.Point(376, 23);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(75, 23);
            this.searchBtn.TabIndex = 5;
            this.searchBtn.Text = "Search";
            this.searchBtn.UseVisualStyleBackColor = true;
            this.searchBtn.Click += new System.EventHandler(this.searchBtn_Click);
            // 
            // getPathBtn
            // 
            this.getPathBtn.Location = new System.Drawing.Point(376, 57);
            this.getPathBtn.Name = "getPathBtn";
            this.getPathBtn.Size = new System.Drawing.Size(75, 23);
            this.getPathBtn.TabIndex = 7;
            this.getPathBtn.Text = "Path";
            this.getPathBtn.UseVisualStyleBackColor = true;
            this.getPathBtn.Click += new System.EventHandler(this.getPathBtn_Click);
            // 
            // cueSarcherMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(463, 209);
            this.Controls.Add(this.getPathBtn);
            this.Controls.Add(this.searchBtn);
            this.Controls.Add(this.searchResGridView);
            this.Controls.Add(this.pathTxtBox);
            this.Controls.Add(this.songNameTxtBox);
            this.Name = "cueSarcherMainForm";
            this.Text = "Cue Searcher beta";
            ((System.ComponentModel.ISupportInitialize)(this.searchResGridView)).EndInit();
            this.searchResMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox songNameTxtBox;
        private System.Windows.Forms.FolderBrowserDialog selectFolderDialog;
        private System.Windows.Forms.TextBox pathTxtBox;
        private System.Windows.Forms.DataGridView searchResGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn Songcol;
        private System.Windows.Forms.DataGridViewTextBoxColumn albumcol;
        private System.Windows.Forms.DataGridViewTextBoxColumn artistcol;
        private System.Windows.Forms.DataGridViewTextBoxColumn pathcol;
        private System.Windows.Forms.ContextMenuStrip searchResMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem folderToolStripMenuItem;
        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.Button getPathBtn;
    }
}

